package com.letshyp.common.security;

public class RestEndPoints {

    public static final String LOGIN ="/api/login";

    public static final String LOGIN_AGENT ="/api/login/agent";

    public static final String LOGIN_ADMIN ="/api/login/admin";

    public static final String REFRESH = "/api/refresh";

    public static final String REGISTER = "/api/register";

    public static final String REGISTER_AGENT = "/api/register/agent";

    public static final String REGISTER_ADMIN = "/api/register/admin";

    public static final String LOGOUT = "/api/logout";
}
