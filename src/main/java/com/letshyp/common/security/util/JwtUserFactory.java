package com.letshyp.common.security.util;


import com.letshyp.common.model.documents.letshypuser.LetshypUser;
import com.letshyp.common.model.objects.letshypuser.LetshypUserRole;
import com.letshyp.common.security.model.JwtUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public final class JwtUserFactory {

    private JwtUserFactory() {
    }

    public static JwtUser create(LetshypUser user) {
        return new JwtUser(
                user.getId(),
                user.getEmail(),
                user.getUserFullName(),
                user.getPassword(),
                mapToGrantedAuthorities(user.getLetshypUserRole()),
                user.isEnabled(),
                new Date(user.getPasswordLastReset())
        );
    }

    public static List<GrantedAuthority> mapToGrantedAuthorities(List<LetshypUserRole> roles) {
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.name()))
                .collect(Collectors.toList());
    }

}
