package com.letshyp.common.security.util;

import com.letshyp.common.model.objects.letshypuser.LetshypUserRole;
import com.letshyp.common.security.RestEndPoints;
import com.letshyp.common.util.CheckNullUtil;

public final class UserUtil {

    public static LetshypUserRole getUserRoleBasedOnSerquestURL(String requestURL){
        if(CheckNullUtil.isEmpty(requestURL)){
            return null;
        }

        if (requestURL.equals(RestEndPoints.REGISTER) || requestURL.equals(RestEndPoints.LOGIN)) {
            return LetshypUserRole.ROLE_CUSTOMER;
        }else if (requestURL.equals(RestEndPoints.REGISTER_AGENT) || requestURL.equals(RestEndPoints.LOGIN_AGENT)) {
            return LetshypUserRole.ROLE_AGENT;
        }else if (requestURL.equals(RestEndPoints.REGISTER_ADMIN) || requestURL.equals(RestEndPoints.LOGIN_ADMIN)) {
            return LetshypUserRole.ROLE_ADMIN;
        }

        return null;
    }

}
