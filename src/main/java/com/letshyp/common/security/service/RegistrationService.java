package com.letshyp.common.security.service;


import com.letshyp.common.model.documents.letshypuser.LetshypUser;
import com.letshyp.common.model.objects.exceptions.LetshypException;
import com.letshyp.common.model.objects.exceptions.UserRegistrationException;
import com.letshyp.common.security.util.PasswordUtil;
import com.letshyp.common.security.util.UserUtil;
import com.letshyp.common.user.service.LetshypUserDetailsService;
import com.letshyp.common.util.CheckNullUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class RegistrationService {

    @Autowired
    LetshypUserDetailsService userDetailsService;

    @Autowired
    private HttpServletRequest httpServletRequest;


    public void registerUser(LetshypUser user) throws UserRegistrationException, LetshypException {
        validateUser(user);
        user.setPassword(PasswordUtil.hashPassword(user.getPassword()));
        userDetailsService.registerUser(user, UserUtil.getUserRoleBasedOnSerquestURL(httpServletRequest.getServletPath()));
    }



    private void validateUser(LetshypUser user) throws UserRegistrationException {

        if (CheckNullUtil.isEmpty(user.getEmail())) {
            throw new UserRegistrationException("email cannot be empty");
        }

        if (CheckNullUtil.isEmpty(user.getPhoneNumber())) {
            throw new UserRegistrationException("Phone Number cannot be empty");
        }
    }


}
