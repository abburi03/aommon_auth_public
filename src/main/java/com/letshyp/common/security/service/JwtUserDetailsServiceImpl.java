package com.letshyp.common.security.service;


import com.letshyp.common.model.documents.letshypuser.LetshypUser;
import com.letshyp.common.model.objects.letshypuser.LetshypUserRole;
import com.letshyp.common.security.model.JwtUser;
import com.letshyp.common.security.util.JwtUserFactory;
import com.letshyp.common.security.util.UserUtil;
import com.letshyp.common.user.service.LetshypUserDetailsService;
import com.letshyp.common.util.CheckNullUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;


@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private LetshypUserDetailsService letshypUserDetailsService;

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Override
    public JwtUser loadUserByUsername(String email) throws UsernameNotFoundException {

        if (CheckNullUtil.isEmpty(email)) {
            throw new UsernameNotFoundException("Bad Credentials");
        }
        LetshypUser user;
        String servletPath = httpServletRequest.getServletPath();
        LetshypUserRole letshypUserRole = UserUtil.getUserRoleBasedOnSerquestURL(servletPath);
        if(!CheckNullUtil.isEmpty(servletPath) && !CheckNullUtil.isEmpty(letshypUserRole)){
            user = letshypUserDetailsService.getUserByEmailBasedonAPICallType(email, letshypUserRole);
        }else {
            user = letshypUserDetailsService.getUserByEmail(email);
        }

        if (user == null) {
            throw new UsernameNotFoundException(String.format("No letshypuser found with username '%s'.", email));
        } else {
            return JwtUserFactory.create(user);
        }
    }

}
