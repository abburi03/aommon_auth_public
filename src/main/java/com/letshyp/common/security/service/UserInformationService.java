package com.letshyp.common.security.service;

import com.letshyp.common.model.documents.letshypuser.LetshypUser;
import com.letshyp.common.model.objects.exceptions.UnAuthorizedAccessException;
import com.letshyp.common.security.util.JwtTokenUtil;
import com.letshyp.common.user.service.LetshypUserDetailsService;
import com.letshyp.common.util.CheckNullUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service("userInformationService")
public class UserInformationService {

    @Autowired
    private HttpServletRequest request;

    @Value("${jwt.header}")
    private String header;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private LetshypUserDetailsService userDetailsService;


    public LetshypUser getCurrentUser() throws UnAuthorizedAccessException {
        String token = request.getHeader(header);
        String userID = jwtTokenUtil.getUserIDFromToken(token);
        LetshypUser user = userDetailsService.getUserByID(userID);

        if (CheckNullUtil.isEmpty(user)) {
            throw new UnAuthorizedAccessException("User may not be logged in");
        }

        return user;
    }
}
