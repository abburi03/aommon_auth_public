package com.letshyp.common.security.service;


import com.letshyp.common.model.documents.letshypuser.LetshypUser;
import com.letshyp.common.model.documents.letshypuser.UserLogins;
import com.letshyp.common.model.objects.JwtAuthenticationRequest;
import com.letshyp.common.model.objects.JwtAuthenticationResponse;
import com.letshyp.common.model.objects.ResponseObject;
import com.letshyp.common.model.objects.exceptions.LetshypException;
import com.letshyp.common.security.model.JwtUser;
import com.letshyp.common.security.util.JwtTokenUtil;
import com.letshyp.common.user.service.LetshypUserDetailsService;
import com.letshyp.common.user.service.LetshypUserLoginDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mobile.device.Device;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class UserAuthenticationHelper {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsServiceImpl userDetailsService;

    @Autowired
    private LetshypUserDetailsService letshypUserDetailsService;

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private LetshypUserLoginDetailsService letshypUserLoginDetailsService;

    @Value("${jwt.header}")
    private String tokenHeader;


    public ResponseObject authenticateUser(JwtAuthenticationRequest authenticationRequest, Device device){
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticationRequest.getEmail(),
                        authenticationRequest.getPassword()
                )
        );



        SecurityContextHolder.getContext().setAuthentication(authentication);

        final JwtUser userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getEmail());
        final String token = jwtTokenUtil.generateToken(userDetails, device);

        LetshypUser user = letshypUserDetailsService.getUserByEmail(userDetails.getEmail());

        ResponseObject<LetshypUser> responseObject = new ResponseObject("success", true, user, new JwtAuthenticationResponse(token));


        letshypUserLoginDetailsService.recordUserLogin(new UserLogins(user.getId(),user.getEmail(), token, device.toString(), letshypUserDetailsService.getLoggedInRole(user) ));
        return responseObject;
    }

    public ResponseObject logout(){
        String authToken = httpServletRequest.getHeader(tokenHeader);
        try {
            jwtTokenUtil.invalidateToken(authToken);
        } catch (LetshypException e) {
          //todo log  e.printStackTrace();
        }
        return new ResponseObject("success", true);

    }

    public String refreshToken(String token){
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser user = userDetailsService.loadUserByUsername(username);
        String refreshedToken = null;
        if (jwtTokenUtil.canTokenBeRefreshed(token, user.getLastPasswordResetDate())) {
            refreshedToken = jwtTokenUtil.refreshToken(token);
        }

        return refreshedToken;
    }
}
