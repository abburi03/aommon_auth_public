package com.letshyp.common.security.controller;


import com.letshyp.common.model.documents.letshypuser.admin.Admin;
import com.letshyp.common.model.documents.letshypuser.agent.Agent;
import com.letshyp.common.model.documents.letshypuser.user.User;
import com.letshyp.common.model.objects.JwtAuthenticationRequest;
import com.letshyp.common.model.objects.ResponseObject;
import com.letshyp.common.model.objects.exceptions.LetshypException;
import com.letshyp.common.model.objects.exceptions.UserRegistrationException;
import com.letshyp.common.security.RestEndPoints;
import com.letshyp.common.security.service.RegistrationService;
import com.letshyp.common.security.service.UserAuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserRegistrationController {


    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private UserAuthenticationHelper userAuthenticationHelper;

    @RequestMapping(value = RestEndPoints.REGISTER, method = RequestMethod.POST)
    public @ResponseBody
    ResponseObject registerUser(@RequestBody User user, Device device){
        try {
            String password = user.getPassword();
            registrationService.registerUser(user);
            return userAuthenticationHelper.authenticateUser(new JwtAuthenticationRequest(user.getEmail(), password), device);
        } catch (UserRegistrationException | LetshypException e) {
            return new ResponseObject(e.getMessage(), false);
        }
    }

    //@PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = RestEndPoints.REGISTER_AGENT, method = RequestMethod.POST)
    public @ResponseBody
    ResponseObject registerAgent(@RequestBody Agent agent, Device device){
        try {
            String password = agent.getPassword();
            registrationService.registerUser(agent);
            return userAuthenticationHelper.authenticateUser(new JwtAuthenticationRequest(agent.getEmail(), password), device);
        } catch (UserRegistrationException | LetshypException e) {
            return new ResponseObject(e.getMessage(), false);
        }
    }

    @RequestMapping(value = RestEndPoints.REGISTER_ADMIN, method = RequestMethod.POST)
    public @ResponseBody
    ResponseObject registerAdmin(@RequestBody Admin admin, Device device){
        try {
            String password = admin.getPassword();
            registrationService.registerUser(admin);
            return userAuthenticationHelper.authenticateUser(new JwtAuthenticationRequest(admin.getEmail(), password), device);


        } catch (UserRegistrationException | LetshypException e) {
            return new ResponseObject(e.getMessage(), false);
        }
    }



}
