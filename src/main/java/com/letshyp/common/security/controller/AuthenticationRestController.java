package com.letshyp.common.security.controller;


import com.letshyp.common.model.objects.JwtAuthenticationRequest;
import com.letshyp.common.model.objects.ResponseObject;
import com.letshyp.common.security.RestEndPoints;
import com.letshyp.common.security.model.JwtUser;
import com.letshyp.common.security.service.UserAuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class AuthenticationRestController {

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private UserAuthenticationHelper userAuthenticationHelper;

    @RequestMapping(value = RestEndPoints.LOGIN, method = RequestMethod.POST)
    public ResponseObject<JwtUser> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest, Device device) throws AuthenticationException {
        return userAuthenticationHelper.authenticateUser(authenticationRequest, device);

    }

    @RequestMapping(value = RestEndPoints.LOGIN_AGENT, method = RequestMethod.POST)
    public ResponseObject<JwtUser> createAuthenticationTokenForAgent(@RequestBody JwtAuthenticationRequest authenticationRequest, Device device) throws AuthenticationException {
        return userAuthenticationHelper.authenticateUser(authenticationRequest, device);

    }

    @RequestMapping(value = RestEndPoints.LOGIN_ADMIN, method = RequestMethod.POST)
    public ResponseObject<JwtUser> createAuthenticationTokenForAdmin(@RequestBody JwtAuthenticationRequest authenticationRequest, Device device) throws AuthenticationException {
        return userAuthenticationHelper.authenticateUser(authenticationRequest, device);

    }

    @RequestMapping(value = RestEndPoints.LOGOUT, method = RequestMethod.GET)
    public ResponseObject<JwtUser> logout() throws AuthenticationException {
        return userAuthenticationHelper.logout();
    }

    @RequestMapping(value = RestEndPoints.REFRESH, method = RequestMethod.GET)
    public ResponseObject refreshAndGetAuthenticationToken(HttpServletRequest request) {
       String token = userAuthenticationHelper.refreshToken(request.getHeader(tokenHeader));
       if(token == null){
           return new ResponseObject("failed to refresh token", false);
       }

       return new ResponseObject("success", true, token);
    }

}
