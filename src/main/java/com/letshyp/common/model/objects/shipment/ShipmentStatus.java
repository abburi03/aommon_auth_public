package com.letshyp.common.model.objects.shipment;

/**
 * Created by PK on 6/24/17.
 */
public enum ShipmentStatus {

    CREATED,
    AWAITING_PICKUP,
    PICKED_UP,
    AT_LETSHYP,
    COMPLETED_BY_AGENT,
    COMPLETED,

}
