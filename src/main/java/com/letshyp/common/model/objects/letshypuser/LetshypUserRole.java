package com.letshyp.common.model.objects.letshypuser;

/**
 * Created by abburi on 6/12/17.
 */
public enum LetshypUserRole {
    ROLE_CUSTOMER,
    ROLE_AGENT,
    ROLE_ADMIN
}
