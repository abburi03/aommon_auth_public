package com.letshyp.common.model.objects.shipment;

import java.io.Serializable;

/**
 * Created by PK on 6/24/17.
 */
public class ServiceProvider implements Serializable {

    private String name;
    private double price;
    private int minDays;
    private int maxDays;
    private String trackingNumber;

}
