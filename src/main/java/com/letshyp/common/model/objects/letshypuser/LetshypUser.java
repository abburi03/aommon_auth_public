package com.letshyp.common.model.objects.letshypuser;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.letshyp.common.model.documents.letshypuser.user.UserAddress;
import com.letshyp.common.model.objects.exceptions.LetshypException;

import java.util.Date;
import java.util.List;

public interface LetshypUser {

    @JsonIgnore
    String getId();

    String getEmail();

    void setEmail(String email);

    @JsonIgnore
    String getPassword();

    @JsonProperty
    void setPassword(String password);

    String getUserFullName();

    void setUserFullName(String userFullName);

    String getPhoneNumber();

    void setPhoneNumber(String phoneNumber);


    Date getCreatedDate();

    void setCreatedDate(Date createdDate);

    @JsonIgnore
    Date getUpdatedDate();

    void setUpdatedDate(Date updatedDate);

    @JsonIgnore
    boolean isEnabled();

    void setEnabled(boolean enabled);

    @JsonIgnore
    long getPasswordLastReset();

    void setPasswordLastReset(long passwordLastReset);

    String getUserPhotoUrl();

    void setUserPhotoUrl(String userPhotoUrl);

    @JsonIgnore
    List<LetshypUserRole> getLetshypUserRole();

    void setLetshypUserRole(List<LetshypUserRole> letshypUserRole);

    List<UserAddress> getUserAddressList();
    
    void setUserAddressList(List<UserAddress> userAddressList);

    @JsonIgnore
    boolean isIsaddedDuetoRecipient();
    
    void setIsaddedDuetoRecipient(boolean isaddedDuetoRecipient);

    @JsonIgnore
    boolean isRegisteredDuetoRecipient();

    void setRegisteredDuetoRecipient(boolean registeredDuetoRecipient);
    @JsonIgnore
    boolean hasRole(LetshypUserRole role);
    @JsonIgnore
    void addRole(LetshypUserRole role) throws LetshypException;
}
