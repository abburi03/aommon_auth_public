package com.letshyp.common.model.objects.exceptions;

/**
 * Created by PK on 6/10/17.
 */
public class SMSException extends Exception {

    public SMSException(String message){
        super(message);
    }

    public SMSException(String message, Throwable cause){
        super(message, cause);
    }

}
