package com.letshyp.common.model.objects.letshypuser;


import com.letshyp.common.model.documents.letshypuser.user.UserAddress;
import com.letshyp.common.model.objects.exceptions.LetshypException;
import com.letshyp.common.util.CheckNullUtil;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by abburi on 6/12/17.
 */

@Document(collection = "admins")
public class Admin implements LetshypUser {


    @Id
    private String id;
    @Indexed
    private String email;
    @Indexed
    private String phoneNumber;
    private String password;
    private String userFullName;
    private Date createdDate;
    private Date updatedDate;
    private boolean enabled = true;
    private long passwordLastReset;
    private String userPhotoUrl;
    private List<LetshypUserRole> letshypUserRole;
    private List<UserAddress> userAddressList;
    private boolean isaddedDuetoRecipient;
    private boolean isRegisteredDuetoRecipient;

    public Admin() {
        if (CheckNullUtil.isEmpty(this.getLetshypUserRole())) {
            letshypUserRole = new ArrayList();
            letshypUserRole.add(LetshypUserRole.ROLE_ADMIN);
            letshypUserRole.add(LetshypUserRole.ROLE_AGENT);
            letshypUserRole.add(LetshypUserRole.ROLE_CUSTOMER);
        }
    }

    @Override
    public boolean hasRole(LetshypUserRole role) {
        return !CheckNullUtil.isEmpty(letshypUserRole) && letshypUserRole.contains(role);
    }


    @Override
    public void addRole(LetshypUserRole role) throws LetshypException {
        if (getLetshypUserRole().contains(role)) {
            throw new LetshypException("Role Already Exists");
        }
        if (CheckNullUtil.isEmpty(this.getLetshypUserRole())) {
            letshypUserRole = new ArrayList();
            letshypUserRole.add(role);
        } else {
            letshypUserRole.add(role);
        }


    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getUserFullName() {
        return userFullName;
    }

    @Override
    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    @Override
    public Date getCreatedDate() {
        return createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public Date getUpdatedDate() {
        return updatedDate;
    }

    @Override
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public long getPasswordLastReset() {
        return passwordLastReset;
    }

    @Override
    public void setPasswordLastReset(long passwordLastReset) {
        this.passwordLastReset = passwordLastReset;
    }

    @Override
    public String getUserPhotoUrl() {
        return userPhotoUrl;
    }

    @Override
    public void setUserPhotoUrl(String userPhotoUrl) {
        this.userPhotoUrl = userPhotoUrl;
    }

    @Override
    public List<LetshypUserRole> getLetshypUserRole() {
        return letshypUserRole;
    }

    @Override
    public void setLetshypUserRole(List<LetshypUserRole> letshypUserRole) {
        this.letshypUserRole = letshypUserRole;
    }

    @Override
    public List<UserAddress> getUserAddressList() {
        return userAddressList;
    }

    @Override
    public void setUserAddressList(List<UserAddress> userAddressList) {
        this.userAddressList = userAddressList;
    }

    @Override
    public boolean isIsaddedDuetoRecipient() {
        return isaddedDuetoRecipient;
    }

    @Override
    public void setIsaddedDuetoRecipient(boolean isaddedDuetoRecipient) {
        this.isaddedDuetoRecipient = isaddedDuetoRecipient;
    }

    @Override
    public boolean isRegisteredDuetoRecipient() {
        return isRegisteredDuetoRecipient;
    }

    @Override
    public void setRegisteredDuetoRecipient(boolean registeredDuetoRecipient) {
        isRegisteredDuetoRecipient = registeredDuetoRecipient;
    }
}
