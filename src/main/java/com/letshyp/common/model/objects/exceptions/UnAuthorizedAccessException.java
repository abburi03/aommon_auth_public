package com.letshyp.common.model.objects.exceptions;

/**
 * Created by abburi on 6/11/17.
 */
public class UnAuthorizedAccessException extends Exception {

    public UnAuthorizedAccessException(String message){
        super(message);
    }

    public UnAuthorizedAccessException(String message, Throwable cause){
        super(message, cause);
    }

}
