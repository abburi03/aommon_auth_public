package com.letshyp.common.model.objects.exceptions;

/**
 * Created by abburi on 6/11/17.
 */
public class UserRegistrationException extends Exception {

    public UserRegistrationException(String message){
        super(message);
    }

    public UserRegistrationException(String message, Throwable cause){
        super(message, cause);
    }

}
