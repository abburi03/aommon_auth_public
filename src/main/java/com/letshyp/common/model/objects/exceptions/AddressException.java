package com.letshyp.common.model.objects.exceptions;

/**
 * Created by abburi on 6/24/17.
 */
public class AddressException extends Exception {

    public AddressException(String message){
        super(message);
    }

    public AddressException(String message, Throwable cause){
        super(message, cause);
    }
}
