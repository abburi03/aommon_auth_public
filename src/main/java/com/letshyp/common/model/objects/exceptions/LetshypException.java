package com.letshyp.common.model.objects.exceptions;

/**
 * Created by abburi on 6/12/17.
 */
public class LetshypException extends Exception {

    public LetshypException(String message){
        super(message);
    }

    public LetshypException(String message, Throwable cause){
        super(message, cause);
    }
}
