package com.letshyp.common.model.objects;

/**
 * Created by abburi on 6/9/17.
 */
public class ResponseObject<T> {

    private String responseMsg;

    private boolean responseStatus;

    private JwtAuthenticationResponse jwtAuthenticationResponse;

    private T messageBody;

    public ResponseObject(String responseMsg, boolean responseStatus, T messageBody){
        this.responseMsg = responseMsg;
        this.responseStatus = responseStatus;
        this.messageBody = messageBody;
    }

    public ResponseObject(boolean responseStatus){
        this.responseStatus = responseStatus;
    }

    public ResponseObject(String responseMsg, boolean responseStatus){
        this.responseMsg = responseMsg;
        this.responseStatus = responseStatus;
    }

    public ResponseObject(boolean responseStatus,  T messageBody){
        this.messageBody = messageBody;
        this.responseStatus = responseStatus;
    }

    public ResponseObject(String responseMsg, boolean responseStatus, T messageBody, JwtAuthenticationResponse jwtAuthenticationResponse){
        this.responseMsg = responseMsg;
        this.responseStatus = responseStatus;
        this.messageBody = messageBody;
        this.jwtAuthenticationResponse = jwtAuthenticationResponse;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public boolean isResponseStatus() {
        return responseStatus;
    }

    public T getMessageBody() {
        return messageBody;
    }

    public JwtAuthenticationResponse getJwtAuthenticationResponse() {
        return jwtAuthenticationResponse;
    }
}
