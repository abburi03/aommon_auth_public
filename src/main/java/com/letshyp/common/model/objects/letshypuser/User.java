package com.letshyp.common.model.objects.letshypuser;


import com.letshyp.common.model.objects.exceptions.LetshypException;
import com.letshyp.common.util.CheckNullUtil;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document(collection = "users")
public class User implements LetshypUser {

    @Id
    private String id;
    @Indexed
    private String email;
    @Indexed
    private String phoneNumber;
    private String password;
    private String userFullName;
    private Date createdDate;
    private Date updatedDate;
    private boolean enabled = true;
    private long passwordLastReset;
    private String userPhotoUrl;
    private List<LetshypUserRole> letshypUserRole;
    private List<UserAddress> userAddressList;
    private boolean isaddedDuetoRecipient;
    private boolean isRegisteredDuetoRecipient;

    public User(){
        if(CheckNullUtil.isEmpty(this.getLetshypUserRole())){
            letshypUserRole = new ArrayList();
            letshypUserRole.add(LetshypUserRole.ROLE_CUSTOMER);
        }
    }

    @Override
    public boolean hasRole(LetshypUserRole role) {
        return !CheckNullUtil.isEmpty(letshypUserRole) && letshypUserRole.contains(role);
    }

    @Override
    public void addRole(LetshypUserRole role) throws LetshypException {
        if(getLetshypUserRole().contains(role)){
            throw new LetshypException("Role Already Exists");
        }
        if(!role.equals(LetshypUserRole.ROLE_CUSTOMER)){
            throw new LetshypException("only a customer role can be added");
        }
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public long getPasswordLastReset() {
        return passwordLastReset;
    }

    public void setPasswordLastReset(long passwordLastReset) {
        this.passwordLastReset = passwordLastReset;
    }

    public String getUserPhotoUrl() {
        return userPhotoUrl;
    }

    public void setUserPhotoUrl(String userPhotoUrl) {
        this.userPhotoUrl = userPhotoUrl;
    }

    @Override
    public List<LetshypUserRole> getLetshypUserRole() {
        return letshypUserRole;
    }

    @Override
    public void setLetshypUserRole(List<LetshypUserRole> letshypUserRole) {
        this.letshypUserRole = letshypUserRole;
    }

    public List<UserAddress> getUserAddressList() {
        return userAddressList;
    }

    public void setUserAddressList(List<UserAddress> userAddressList) {
        this.userAddressList = userAddressList;
    }

    public boolean isIsaddedDuetoRecipient() {
        return isaddedDuetoRecipient;
    }

    public void setIsaddedDuetoRecipient(boolean isaddedDuetoRecipient) {
        this.isaddedDuetoRecipient = isaddedDuetoRecipient;
    }

    public boolean isRegisteredDuetoRecipient() {
        return isRegisteredDuetoRecipient;
    }

    public void setRegisteredDuetoRecipient(boolean registeredDuetoRecipient) {
        isRegisteredDuetoRecipient = registeredDuetoRecipient;
    }
}
