package com.letshyp.common.model.objects.notifications;

/**
 * Created by abburi on 6/21/17.
 */
public class NotificationModel<T> {

    private long producedTimeStamp;

    private T payload;

    public NotificationModel(){}

    public NotificationModel(T payload){
        this.producedTimeStamp = System.currentTimeMillis();
        this.payload = payload;
    }

    public long getProducedTimeStamp() {
        return producedTimeStamp;
    }

    public T getPayload() {
        return payload;
    }

    @Override
    public String toString() {
        return "NotificationModel{" +
                "producedTimeStamp=" + producedTimeStamp +
                ", payload=" + payload +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NotificationModel<?> that = (NotificationModel<?>) o;

        if (producedTimeStamp != that.producedTimeStamp) return false;
        return payload != null ? payload.equals(that.payload) : that.payload == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (producedTimeStamp ^ (producedTimeStamp >>> 32));
        result = 31 * result + (payload != null ? payload.hashCode() : 0);
        return result;
    }
}
