package com.letshyp.common.model.objects.notifications;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.letshyp.common.model.objects.letshypuser.vo.AddressVO;

/**
 * Created by abburi on 6/22/17.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserShipmentNotification implements UserNotifications {

    private String shipmentID;

    private String shipmentTitle;

    private String phoneNumber;

    private AddressVO addressVO;

    private String agentName;

    private String agentPhoneNumber;

    private String agentPhotoURL;


    public UserShipmentNotification() {
    }

    public UserShipmentNotification(String shipmentID, String shipmentTitle, String phoneNumber, AddressVO addressVO, String agentName, String agentPhoneNumber, String agentPhotoURL) {
        this.shipmentID = shipmentID;
        this.shipmentTitle = shipmentTitle;
        this.phoneNumber = phoneNumber;
        this.addressVO = addressVO;
        this.agentName = agentName;
        this.agentPhoneNumber = agentPhoneNumber;
        this.agentPhotoURL = agentPhotoURL;
    }

    public String getShipmentID() {
        return shipmentID;
    }

    public String getShipmentTitle() {
        return shipmentTitle;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public AddressVO getAddressVO() {
        return addressVO;
    }

    public String getAgentName() {
        return agentName;
    }

    public String getAgentPhoneNumber() {
        return agentPhoneNumber;
    }

    public String getAgentPhotoURL() {
        return agentPhotoURL;
    }

    public static class UserShipmentNotificationBuilder {
        private String shipmentID;

        private String shipmentTitle;

        private String userPhoneNumber;

        private AddressVO pickUpAddress;

        private String agentName;

        private String agentPhoneNumber;

        private String agentPhotoURL;

        public UserShipmentNotificationBuilder shipmentID(String shipmentID) {
            this.shipmentID = shipmentID;
            return this;
        }

        public UserShipmentNotificationBuilder shipmentTitle(String shipmentTitle) {
            this.shipmentTitle = shipmentTitle;
            return this;
        }

        public UserShipmentNotificationBuilder userPhoneNumber(String userPhoneNumber) {
            this.userPhoneNumber = userPhoneNumber;
            return this;
        }

        public UserShipmentNotificationBuilder pickUpAddress(AddressVO pickUpAddress) {
            this.pickUpAddress = pickUpAddress;
            return this;
        }

        public UserShipmentNotificationBuilder agentName(String agentName) {
            this.agentName = agentName;
            return this;
        }

        public UserShipmentNotificationBuilder agentPhoneNumber(String agentPhoneNumber) {
            this.agentPhoneNumber = agentPhoneNumber;
            return this;
        }

        public UserShipmentNotificationBuilder agentPhotoURL(String agentPhotoURL) {
            this.agentPhotoURL = agentPhotoURL;
            return this;
        }


        public UserShipmentNotification build() {
            return new UserShipmentNotification(shipmentID, shipmentTitle, userPhoneNumber, pickUpAddress, agentName, agentPhoneNumber, agentPhotoURL);
        }
    }
}