package com.letshyp.common.model.objects.letshypuser.vo;

import java.io.Serializable;

/**
 * Created by abburi on 6/24/17.
 */
public class PasswordVO implements Serializable{
    private String existingPassword;
    private String newPassword;
    private String reNewPassword;

    public String getExistingPassword() {
        return existingPassword;
    }

    public void setExistingPassword(String existingPassword) {
        this.existingPassword = existingPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getReNewPassword() {
        return reNewPassword;
    }

    public void setReNewPassword(String reNewPassword) {
        this.reNewPassword = reNewPassword;
    }

}
