package com.letshyp.common.model.objects.exceptions;

/**
 * Created by abburi on 6/24/17.
 */
public class UserNotFoundException extends Exception{

    public UserNotFoundException(String message){
        super(message);
    }
}
