package com.letshyp.common.model.objects.letshypuser.vo;

import java.io.Serializable;

/**
 * Created by PK on 6/24/17.
 */
public class UserVO implements Serializable {

    private String email;
    private String phoneNumber;
    private String name;
    private String photoURL;

    public UserVO() {
    }

    public UserVO(String email, String phoneNumber, String name) {
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.name = name;
    }

    public UserVO(String email, String phoneNumber, String name, String photoURL) {
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.name = name;
        this.photoURL = photoURL;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getPhotoURL() {
        return photoURL;
    }
}
