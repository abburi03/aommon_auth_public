package com.letshyp.common.model.objects.exceptions;

/**
 * Created by abburi on 6/10/17.
 */
public class AuthenticationException extends Exception {

    public AuthenticationException(String message){
        super(message);
    }

    public AuthenticationException(String message, Throwable cause){
        super(message, cause);
    }
}
