package com.letshyp.common.model.objects.notifications;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.letshyp.common.model.objects.letshypuser.vo.AddressVO;
import com.letshyp.common.model.objects.shipment.ShipmentSize;

import java.util.Date;

/**
 * Created by abburi on 6/22/17.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AgentShipmentNotification implements AgentNotifications{

    private String userFullName;

    private String userPhoneNumber;

    private AddressVO pickUpAddress;

    private Date pickUpRequestTime;

    private String pickupInstructions;

    private String shipmentID;

    private ShipmentSize shipmentSize;

    public AgentShipmentNotification(){}

     AgentShipmentNotification(String userFullName, String userPhoneNumber, AddressVO pickUpAddress, Date pickUpRequestTime, String pickupInstructions, String shipmentID, ShipmentSize shipmentSize) {
        this.userFullName = userFullName;
        this.userPhoneNumber = userPhoneNumber;
        this.pickUpAddress = pickUpAddress;
        this.pickUpRequestTime = pickUpRequestTime;
        this.pickupInstructions = pickupInstructions;
        this.shipmentID = shipmentID;
        this.shipmentSize = shipmentSize;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public String getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public AddressVO getPickUpAddress() {
        return pickUpAddress;
    }

    public Date getPickUpRequestTime() {
        return pickUpRequestTime;
    }

    public String getPickupInstructions() {
        return pickupInstructions;
    }

    public String getShipmentID() {
        return shipmentID;
    }

    public ShipmentSize getShipmentSize() {
        return shipmentSize;
    }



    @Override
    public String toString() {
        return "AgentShipmentNotification{" +
                "userFullName='" + userFullName + '\'' +
                ", userPhoneNumber='" + userPhoneNumber + '\'' +
                ", pickUpAddress=" + pickUpAddress +
                ", pickUpRequestTime=" + pickUpRequestTime +
                ", pickupInstructions='" + pickupInstructions + '\'' +
                ", shipmentID='" + shipmentID + '\'' +
                ", shipmentSize=" + shipmentSize +
                '}';
    }

    public static class AgentShipmentNotificationBuilder {
        private String userFullName;

        private String userPhoneNumber;

        private AddressVO pickUpAddress;

        private Date pickUpRequestTime;

        private String pickupInstructions;

        private String shipmentID;

        private ShipmentSize shipmentSize;

        public AgentShipmentNotificationBuilder userFullName(String userFullName){
            this.userFullName = userFullName;
            return this;
        }

        public AgentShipmentNotificationBuilder userPhoneNumber(String userPhoneNumber){
            this.userPhoneNumber = userPhoneNumber;
            return this;
        }

        public AgentShipmentNotificationBuilder pickupInstructions(String pickupInstructions){
            this.pickupInstructions = pickupInstructions;
            return this;
        }

        public AgentShipmentNotificationBuilder pickUpAddress(AddressVO pickUpAddress){
            this.pickUpAddress = pickUpAddress;
            return this;
        }

        public AgentShipmentNotificationBuilder pickUpRequestTime(Date pickUpRequestTime){
            this.pickUpRequestTime = pickUpRequestTime;
            return this;
        }

        public AgentShipmentNotificationBuilder shipmentID(String shipmentID){
            this.shipmentID = shipmentID;
            return this;
        }

        public AgentShipmentNotificationBuilder shipmentSize(ShipmentSize shipmentSize){
            this.shipmentSize = shipmentSize;
            return this;
        }

        public AgentShipmentNotification build(){
            return new AgentShipmentNotification(userFullName, userPhoneNumber, pickUpAddress, pickUpRequestTime, pickupInstructions, shipmentID, shipmentSize);
        }


    }
}
