package com.letshyp.common.model.objects.exceptions;

import com.letshyp.common.util.CheckNullUtil;

/**
 * Created by abburi on 6/26/17.
 */
public class ShipmentNotFoundException extends Exception {

    public ShipmentNotFoundException(String message){
        super(CheckNullUtil.isEmpty(message) ? "Shipment Not found" : message);
    }

}
