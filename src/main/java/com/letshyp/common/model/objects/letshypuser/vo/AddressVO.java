package com.letshyp.common.model.objects.letshypuser.vo;

import java.io.Serializable;

/**
 * Created by PK on 6/24/17.
 */
public class AddressVO implements Serializable {

    private String addressLine;
    private String addressLineOther;
    private String city;
    private String country;
    private String state;
    private String zipCode;
    private String latitude;
    private String longitude;
    private double[] location;

}
