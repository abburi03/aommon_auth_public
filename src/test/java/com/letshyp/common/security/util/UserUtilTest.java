package com.letshyp.common.security.util;

import com.letshyp.common.model.objects.letshypuser.LetshypUserRole;
import com.letshyp.common.security.RestEndPoints;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class UserUtilTest {

    @Test
    public void testGetUserRoleBasedOnSerquestURL(){
        assertEquals(UserUtil.getUserRoleBasedOnSerquestURL(null), null);
        assertEquals(UserUtil.getUserRoleBasedOnSerquestURL("/someRandomURL"), null);

        assertEquals(UserUtil.getUserRoleBasedOnSerquestURL(RestEndPoints.LOGIN), LetshypUserRole.ROLE_CUSTOMER);
        assertEquals(UserUtil.getUserRoleBasedOnSerquestURL(RestEndPoints.REGISTER), LetshypUserRole.ROLE_CUSTOMER);

        assertEquals(UserUtil.getUserRoleBasedOnSerquestURL(RestEndPoints.LOGIN_AGENT), LetshypUserRole.ROLE_AGENT);
        assertEquals(UserUtil.getUserRoleBasedOnSerquestURL(RestEndPoints.REGISTER_AGENT), LetshypUserRole.ROLE_AGENT);

        assertEquals(UserUtil.getUserRoleBasedOnSerquestURL(RestEndPoints.LOGIN_ADMIN), LetshypUserRole.ROLE_ADMIN);
        assertEquals(UserUtil.getUserRoleBasedOnSerquestURL(RestEndPoints.REGISTER_ADMIN), LetshypUserRole.ROLE_ADMIN);

    }

}
