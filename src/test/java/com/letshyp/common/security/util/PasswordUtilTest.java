package com.letshyp.common.security.util;

import com.letshyp.common.model.objects.exceptions.AuthenticationException;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class PasswordUtilTest {


    @Test
    public void testCheckPassword() throws AuthenticationException {
        String password = "krishna";
        String hashedPassword = PasswordUtil.hashPassword(password);
       assertTrue(PasswordUtil.checkPassword(password, hashedPassword));
        assertTrue(hashedPassword.startsWith("$2a$"));
       assertFalse(PasswordUtil.checkPassword("Krishna", hashedPassword));
    }
}
